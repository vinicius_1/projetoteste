from django.http import HttpResponse

def ola (request):
    return HttpResponse("Hello World")

def listaPessoa(nome) :
    lista_pessoas = [
        {"Nome": "Ana", "idade": 20},
        {"Nome": "João", "idade": 25},
        {"Nome": 'Lucas', "idade": 30}
    ]
    for pessoa in lista_pessoas:
        if pessoa ["Nome"] == nome:
            return pessoa
    else :
        return {"Nome": "O nome não foi encontrado", "idade" : 0}


def fname (request, nome):
    result = listaPessoa(nome)
    if result["idade"] > 0:
        return HttpResponse("O nome é " + result["Nome"] + " e a idade " + str(result["idade"] ) )
    else :
        return HttpResponse("O nome não esta cadastrado")
